from flask import Flask, request, current_app, jsonify
from flask_jwt_extended import JWTManager
from flask_limiter import Limiter
from flask_cors import CORS
from flask_socketio import SocketIO
from apis.config.config import conn, IS_KEEP_SCANNING_PENDING_JOB_KEY, IS_KEEP_SCANNING_PENDING_JOB_VALUE, \
    MAXIMUM_CLOUD_DRIVER_ATTEMPT_VALUE, MAXIMUM_CLOUD_DRIVER_ATTEMPT_KEY, \
    CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_KEY, CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_VALUE
from apis.model.User import User
from constants.constants import Constants
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from utils.log_utils import get_logger_format, get_timed_log_file
from .model.User import db as user_db
from .model.ProjectPermission import db as project_permission_db
from .model.Project import db as project_db
from .model.Application import db as app_db
from .model.Capability import db as cap_db
from .model.ExecutionQueue import db as queue_db
from .model.Execution import db as execution_db
from .model.TestPackage import db as test_package_db
from .model.Config import db as config_db, Config

socketio_manager = SocketIO()
jwt = JWTManager()


def create_app():
    """Create an application."""
    application = Flask(__name__)
    application.config.from_pyfile('config/config.py')
    register_blueprints(application)
    register_error_handlers(application)
    initialize_extensions(application)
    socketio_manager.init_app(application, cors_allowed_origins="*", async_mode='gevent', engineio_logger=False, logger=False,)
    configure_logging(application)
    return application


def register_blueprints(application):
    from .auth import auth_blueprint
    from .user import user_blueprint
    from .capability import capability_blueprint
    from .device import device_blueprint
    from .execution import execution_blueprint
    from .mobile_application import mobile_application_blueprint
    from .project import project_blueprint
    from .test_package import test_package_blueprint
    from .socket import socket_blueprint
    from .deployment import deployment_blueprint
    application.register_blueprint(auth_blueprint, url_prefix="/api")
    application.register_blueprint(user_blueprint, url_prefix="/api")
    application.register_blueprint(capability_blueprint, url_prefix="/api")
    application.register_blueprint(device_blueprint, url_prefix="/api")
    application.register_blueprint(execution_blueprint, url_prefix="/api")
    application.register_blueprint(mobile_application_blueprint, url_prefix="/api")
    application.register_blueprint(project_blueprint, url_prefix="/api")
    application.register_blueprint(test_package_blueprint, url_prefix="/api")
    application.register_blueprint(deployment_blueprint, url_prefix="/api")
    application.register_blueprint(socket_blueprint)


def initialize_extensions(application):
    limiter = Limiter(
        application,
        key_func=get_limiter_ip,
        default_limits=["2000 per second, 10000 per minute, 500000 per hour, 10000000 per day"]
    )
    CORS(application)
    jwt.init_app(application)
    user_db.init_app(application)
    project_permission_db.init_app(application)
    project_db.init_app(application)
    app_db.init_app(application)
    test_package_db.init_app(application)
    cap_db.init_app(application)
    queue_db.init_app(application)
    execution_db.init_app(application)
    config_db.init_app(application)
    with application.app_context():
        user_db.create_all()
        config_db.create_all()
        project_permission_db.create_all()
        project_db.create_all()
        app_db.create_all()
        test_package_db.create_all()
        cap_db.create_all()
        execution_db.create_all()
        queue_db.create_all()
        is_scanning: Config = Config.query.filter_by(name=IS_KEEP_SCANNING_PENDING_JOB_KEY).all()
        if not is_scanning or len(is_scanning) == 0:
            config_db.session.add(Config(
                name=IS_KEEP_SCANNING_PENDING_JOB_KEY,
                value=IS_KEEP_SCANNING_PENDING_JOB_VALUE,
            ))
            config_db.session.add(Config(
                name=MAXIMUM_CLOUD_DRIVER_ATTEMPT_KEY,
                value=MAXIMUM_CLOUD_DRIVER_ATTEMPT_VALUE,
            ))
            config_db.session.add(Config(
                name=CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_KEY,
                value=CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_VALUE,
            ))
            config_db.session.commit()

    # Register a callback function that takes whatever object is passed in as the
    # identity when creating JWTs and converts it to a JSON serializable format.
    @jwt.user_identity_loader
    def user_identity_lookup(user):
        return user.user_id

    # Register a callback function that loades a user from your database whenever
    # a protected route is accessed. This should return any python object on a
    # successful lookup, or None if the lookup failed for any reason (for example
    # if the user has been deleted from the database).
    @jwt.user_lookup_loader
    def user_lookup_callback(_jwt_header, jwt_data):
        user_id = jwt_data["sub"]
        return User.query.filter_by(user_id=user_id).one_or_none()


def get_limiter_ip():
    """
    :return: the ip address for the current key (or 127.0.0.1 if none found)
    """
    return request.environ.get(Constants.IP_HEADER, request.remote_addr) or '127.0.0.1'


def configure_logging(application):
    import logging

    # Create a file handler object
    file_handler = get_timed_log_file("flask")

    # Set the logging level of the file handler object so that it logs DEBUG and up
    file_handler.setLevel(logging.DEBUG)

    # Set the logging level of the application logger instance
    application.logger.setLevel(logging.DEBUG)

    # Apply the file formatter object to the file handler object
    file_handler.setFormatter(get_logger_format())

    # Add file handler object to the logger
    application.logger.addHandler(file_handler)


def register_error_handlers(application):
    @application.errorhandler(Exception)
    def handle_error(e):
        conn.rollback()
        current_app.logger.critical("Rollback db connection")
        current_app.logger.critical("------------Exception------------")
        current_app.logger.critical(e)
        current_app.logger.critical("------------Request Full Path------------")
        current_app.logger.critical(request.full_path)
        current_app.logger.critical("------------Request Headers------------")
        current_app.logger.critical(request.headers)
        current_app.logger.critical("------------Request Data------------")
        current_app.logger.critical(request.date)
        current_app.logger.critical("------------Request Address------------")
        current_app.logger.critical(request.remote_addr)
        return jsonify({
            ResponseKey.description: ResponseValue.server_error
        }), ResponseStatus.server_error
