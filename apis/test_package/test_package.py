import shutil
import zipfile
from typing import List

from eventlet.green import subprocess
import sys
from flask import Flask, request, current_app
from gherkin.parser import Parser
from gherkin.token_scanner import TokenScanner
from sqlalchemy import desc, delete, and_, or_
from werkzeug.utils import secure_filename
import os
from ability.project_ability import ProjectAbility
from download import download
from status.test_package_status import TestPackageStatus
from utils import permission_utils
from flask_jwt_extended import jwt_required
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from utils import utils
from constants.constants import Constants
from utils.utils import return_json
from . import test_package_blueprint
from ..model.TestPackage import db as test_package_table, TestPackage


@test_package_blueprint.route('/uploadTestPackage', methods=['POST'])
@jwt_required()
def create_upload_test_package():

    def loop_feature_directory(directory: str):
        feature_files = []
        for root, subdirectories, files in os.walk(directory):
            for subdirectory in subdirectories:
                loop_feature_directory(subdirectory)
            for f in files:
                if f.endswith(".feature"):
                    feature_files.append(os.path.join(root, f))
        return feature_files

    def parse_all_feature_files(files: List):
        list_features = []
        for file in files:
            cnt = {}
            f = file.split("/features/")
            parser = Parser()
            feature_file = parser.parse(TokenScanner(file))
            feature = feature_file['feature']
            children = feature['children']
            for child in children:
                scenario = child['scenario']
                examples = scenario["examples"]
                number_of_example = 1
                for example in examples:
                    number_of_example = len(example["tableBody"])
                tags = scenario["tags"]
                for tag in tags:
                    tag_name = tag["name"]
                    if tag_name in cnt:
                        cnt[tag_name] += number_of_example
                    else:
                        cnt[tag_name] = number_of_example
            list_features.append({
                "path": f[-1],
                "count": cnt
            })
        current_app.logger.info(f"Parsed features: {list_features}")
        return list_features

    if Key.zip_file not in request.files:
        return return_json(
            ResponseStatus.empty_file,
            ResponseValue.empty_file
        )
    if Key.project_id not in request.form:
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    zip_file = request.files[Key.zip_file]
    project_id = request.form[Key.project_id]
    user_id = request.form[Key.user_id]
    description = request.form[Key.description]
    test_package_id = utils.get_uuid()
    upload_id = utils.get_uuid()
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Create().upload_test_package):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if not zip_file.filename:
        current_app.logger.info(f"Upload new test package with invalid file name {zip_file.filename}, requested by {user_id}")
        return return_json(
            ResponseStatus.empty_file,
            ResponseValue.empty_file
        )
    if zip_file and utils.allowed_file(zip_file.filename):
        # The name of the test package
        zip_filename = secure_filename(zip_file.filename)
        test_package = Constants.DEFAULT_TEST_PACKAGE

        # The zip file path that ends in .zip
        zip_file_path = os.path.join(download.file_folder, zip_filename)
        zip_file.save(zip_file_path)

        # The temp folder to unzip the test package
        temp_unzip_path = download.file_folder + "/temp"

        # Unzip the package into the temp folder
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            zip_ref.extractall(temp_unzip_path)

        # Loop through all feature files in the directory
        all_feature_files = loop_feature_directory(temp_unzip_path + Constants.DEFAULT_FEATURE_FOLDER_PATH)

        # Parse all feature files found and save the result in features variable
        features = parse_all_feature_files(all_feature_files)

        # Remove the temp folder
        shutil.rmtree(temp_unzip_path)
        file_size = utils.get_file_size(zip_file_path)
        uploaded_s3_file_path = utils.get_s3_upload_path(project_id, test_package, upload_id, zip_filename)
        current_app.logger.info(f"Begin uploading test package {zip_file_path} to {Constants.DEFAULT_BUCKET} as {uploaded_s3_file_path}")
        subprocess.Popen([sys.executable, './upload_to_s3.py', zip_file_path, Constants.DEFAULT_BUCKET, uploaded_s3_file_path,
                          "update test_packages set status = {status} where test_package_id = '{test_package_id}'".format(status=TestPackageStatus.normal, test_package_id=test_package_id),
                          current_app.config['SCRIPT_SECRET_KEY'], project_id, "test_package_status_update", test_package_id])
        current_app.logger.info(f"Successfully started upload test package command")
        new_package = TestPackage(
            test_package_id=test_package_id,
            upload_id=upload_id,
            project_id=project_id,
            file_name=zip_filename,
            user_id=user_id,
            file_path=uploaded_s3_file_path,
            file_size=file_size,
            features=features,
            status=TestPackageStatus.uploading,
            description=description,
        )
        test_package_table.session.add(new_package)
        test_package_table.session.commit()
        inserted_package = TestPackage.query.filter_by(test_package_id=test_package_id).one_or_none()
        return return_json(
            ResponseStatus.success,
            ResponseValue.upload_test_package_success,
            {ResponseKey.data: inserted_package.as_dict()},
        )
    return return_json(
        ResponseStatus.bad_request,
        ResponseValue.malformed_request,
    )


@test_package_blueprint.route('/getTestPackages', methods=['POST'])
@jwt_required()
def get_test_packages():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_test_package):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get test packages of project id {project_id}, requested by {user_id}")
    test_packages = TestPackage.query.filter(and_(TestPackage.project_id == project_id, or_(TestPackage.status == TestPackageStatus.normal, TestPackage.status == TestPackageStatus.uploading))).order_by(desc(TestPackage.created_at)).all()
    test_packages = [tp.as_dict() for tp in test_packages]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_test_packages_success,
        {ResponseKey.data: test_packages},
    )


@test_package_blueprint.route('/deletePackages', methods=['POST'])
@jwt_required()
def delete_packages():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.test_package_ids,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    test_package_ids = posted_json[Key.test_package_ids]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_test_package):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    test_package_ids = test_package_ids.strip('][').split(', ')
    current_app.logger.info(f"Delete {len(test_package_ids)} test packages, requested by {user_id}")
    target_packages = TestPackage.query.filter(TestPackage.test_package_id.in_(test_package_ids)).all()
    for package in target_packages:
        package.status = TestPackageStatus.deleted
    test_package_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_test_package_success,
    )


@test_package_blueprint.route('/deletePackage', methods=['POST'])
@jwt_required()
def delete_package():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.test_package_id,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    test_package_id = posted_json[Key.test_package_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_test_package):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Delete test package of package id {test_package_id}, requested by {user_id}")
    target_package = TestPackage.query.filter_by(test_package_id=test_package_id).first()
    target_package.status = TestPackageStatus.deleted
    test_package_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_test_package_success,
    )
