import requests
from flask_cors import CORS
from werkzeug.exceptions import HTTPException
from flask import Flask, request, jsonify, Response, current_app
from werkzeug.utils import secure_filename
import os

from ability.project_ability import ProjectAbility
from status.app_status import AppStatus
from status.capability_status import CapabilityStatus
from status.project_status import ProjectStatus
from status.test_package_status import TestPackageStatus
from utils import permission_utils
import string
import random
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity, get_jwt
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from status.user_status import UserStatus
from utils import utils
from constants.constants import Constants
from utils.utils import return_json
from . import device_blueprint


@device_blueprint.route('/getListDevices', methods=['POST'])
@jwt_required()
def get_list_devices():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_device):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get list devices of project id: {project_id}")
    r = requests.get(Constants.SAUCELAB_BASE_URL + "v1/rdc/devices",
                     auth=(current_app.config['SAUCELAB_USERNAME'], current_app.config['SAUCELAB_ACCESS_KEY']))
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_devices_success,
        {ResponseKey.data: r.text},
    )


@device_blueprint.route('/getDevice', methods=['POST'])
@jwt_required()
def get_device():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id, Key.device_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    device_id = posted_json[Key.device_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_device):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get device id of {device_id}, by {user_id}")
    r = requests.get(f"{Constants.SAUCELAB_BASE_URL}v1/rdc/devices/{device_id}",
                     auth=(current_app.config['SAUCELAB_USERNAME'], current_app.config['SAUCELAB_ACCESS_KEY']))
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_device_success,
        {ResponseKey.data: r.text},
    )
