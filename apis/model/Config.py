import datetime

from sqlalchemy import func, Column, JSON, Text, Integer, DateTime, Boolean
from status.project_status import ProjectStatus
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Config(db.Model):
    __tablename__ = 'configs'
    name = Column(Text, nullable=False, primary_key=True)
    value = Column(Integer, nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
