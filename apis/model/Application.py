import datetime
import json
from dataclasses import dataclass
from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from status.app_status import AppStatus
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Application(db.Model):
    __tablename__ = 'app_files'
    app_id = Column(Text, primary_key=True, nullable=False)
    upload_id = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    file_name = Column(Text, nullable=False)
    app_version = Column(Text, nullable=False)
    app_name = Column(Text, nullable=False)
    kind = Column(Text, nullable=False)
    file_size = Column(Text, nullable=True)
    app_identifier = Column(Text, nullable=True)
    file_path = Column(Text, nullable=False)
    user_id = Column(Text, nullable=False)
    status = Column(Integer, nullable=False, default=AppStatus.normal)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
