from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from flask_sqlalchemy import SQLAlchemy
import datetime

db = SQLAlchemy()


class Execution(db.Model):
    __tablename__ = 'executions'
    execution_id = Column(Text, nullable=False, primary_key=True)
    batch_id = Column(Text, nullable=False)
    saucelab_test_id = Column(Text, nullable=True)
    video_path = Column(Text, nullable=True)
    excel_report_path = Column(Text, nullable=True)
    word_report_path = Column(Text, nullable=True)
    console_log_path = Column(Text, nullable=True)
    elk_json_path = Column(Text, nullable=True)
    status = Column(Text, nullable=True)
    pass_scenario = Column(Integer, nullable=True)
    fail_scenario = Column(Integer, nullable=True)
    total_scenarios = Column(Integer, nullable=False)
    execution_name = Column(Text, nullable=False)
    capability_id = Column(Text, nullable=False)
    user_id = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    test_package_id = Column(Text, nullable=False)
    app_id = Column(Text, nullable=False)
    app_name = Column(Text, nullable=False)
    app_file_name = Column(Text, nullable=False)
    capability_name = Column(Text, nullable=False)
    test_package_file_name = Column(Text, nullable=False)
    kind = Column(Text, nullable=False)
    test_env = Column(Text, nullable=False)
    data_file_name = Column(Text, nullable=False)
    feature_file_name = Column(Text, nullable=False)
    mobile_properties_profile = Column(Text, nullable=False)
    execution_tag = Column(Text, nullable=False)
    platform_version = Column(Text, nullable=False)
    device_name = Column(Text, nullable=False)
    device_id = Column(Text, nullable=False)
    executed_at = Column(DateTime, nullable=True)
    finished_at = Column(DateTime, nullable=True)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
