import datetime
from os import environ, path
import psycopg2 as ps
import boto3
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

FLASK_ENV = 'production'
DEVELOPMENT = False
IS_KEEP_SCANNING_PENDING_JOB_KEY = "IS_KEEP_SCANNING_PENDING_JOB"
IS_KEEP_SCANNING_PENDING_JOB_VALUE = 1
MAXIMUM_CLOUD_DRIVER_ATTEMPT_KEY = "MAXIMUM_CLOUD_DRIVER_ATTEMPT"
MAXIMUM_CLOUD_DRIVER_ATTEMPT_VALUE = 10
CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_KEY = "CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS"
CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_VALUE = 300
SALT = environ.get('SALT')
if DEVELOPMENT:
    DBNAME = environ.get('LOCALDBNAME')
    DBUSER = environ.get('LOCALDBUSER')
    DBPW = environ.get('LOCALDBPW')
    DBHOST = environ.get('LOCALDBHOST')
    DBPORT = environ.get('LOCALDBPORT')
else:
    DBNAME = environ.get('AWSDBNAME')
    DBUSER = environ.get('AWSDBUSER')
    DBPW = environ.get('AWSDBPW')
    DBHOST = environ.get('AWSDBHOST')
    DBPORT = environ.get('AWSDBPORT')
SQLALCHEMY_DATABASE_URI = f"postgresql://{DBUSER}:{DBPW}@{DBHOST}:{DBPORT}/{DBNAME}"
SCRIPT_SECRET_KEY = environ.get('SCRIPT_SECRET_KEY')
AWS_ACCESS_KEY = environ.get('AWS_ACCESS_KEY')
AWS_SECRET_KEY = environ.get('AWS_SECRET_KEY')
AWS_REGION = "ap-east-1"
DEPLOY_KEY = environ.get('DEPLOY_KEY')
JWT_TOKEN_LOCATION = ('headers')
JWT_SECRET_KEY = environ.get('JWT_SECRET_KEY')
JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(hours=1)
SQLALCHEMY_TRACK_MODIFICATIONS = False
SAUCELAB_REGION = "us-west-1"
SAUCELAB_USERNAME = environ.get('SAUCELAB_USERNAME')
SAUCELAB_ACCESS_KEY = environ.get('SAUCELAB_ACCESS_KEY')
conn = ps.connect(database=DBNAME, host=DBHOST,
                  port=DBPORT, user=DBUSER,
                  password=DBPW)


s3_client = boto3.client('s3', region_name=AWS_REGION, aws_access_key_id=AWS_ACCESS_KEY,
                         aws_secret_access_key=AWS_SECRET_KEY, config=boto3.session.Config(s3={'addressing_style': 'virtual'}, signature_version='s3v4'))


