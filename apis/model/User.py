import datetime
import json
from dataclasses import dataclass
from sqlalchemy import func, Column, JSON, Text, Integer, DateTime

from status.role_status import RoleStatus
from status.user_status import UserStatus
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'users'
    user_id = Column(Text, primary_key=True, nullable=False)
    username = Column(Text, nullable=False, unique=True)
    password = Column(Text, nullable=False)
    permissions = Column(JSON, nullable=False)
    description = Column(Text)
    type = Column(Integer, nullable=False, default=RoleStatus.normal)
    upstream_admin_id = Column(Text, nullable=True)
    status = Column(Integer, nullable=False, default=UserStatus.normal)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
