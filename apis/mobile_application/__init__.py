from flask import Blueprint

mobile_application_blueprint = Blueprint('mobile_application', __name__)

from . import mobile_application
