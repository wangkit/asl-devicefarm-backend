class GlobalAbility:
    # Create
    class Create:
        create_project = "create_project"
        create_user = "create_user"

    # Delete
    class Delete:
        delete_user = "delete_user"

    all_abilities: list = [Create().create_project, Create().create_user, Delete().delete_user]
    none: list = []
