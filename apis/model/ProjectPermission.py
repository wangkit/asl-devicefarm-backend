import json
from dataclasses import dataclass
import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, Column, Text, DateTime


db = SQLAlchemy()


class ProjectPermission(db.Model):
    __tablename__ = 'project_permissions'
    permission_id = Column(Text, primary_key=True)
    user_id = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    ability = Column(Text, nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

