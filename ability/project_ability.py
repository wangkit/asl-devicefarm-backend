import ast
import json


class ProjectAbility:
    # Admin
    class Admin:
        get_assign_users = "get_assign_users"
        get_project_user = "get_project_user"
        assign_user_to_project = "assign_user_to_this_project"
        remove_user_from_project = "remove_user_from_project"
        all_admin_ability: list = [get_assign_users, get_project_user, assign_user_to_project, remove_user_from_project]

    # Read
    class Read:
        read_execution_result = "read_execution_result"
        read_application = "read_application"
        read_test_package = "read_test_package"
        read_device = "read_device"
        read_capability = "read_capability"
        read_project = "read_project"
        download_test_video = "download_test_video"
        download_test_report = "download_test_report"
        download_test_log = "download_test_log"
        all_read_ability: list = [read_project, read_test_package, read_execution_result, read_application, read_device,
                                  read_capability, download_test_log, download_test_report, download_test_video]
        default_user_read_ability: list = [read_project, read_test_package, read_execution_result, read_application, read_device,
                                  read_capability, download_test_report, download_test_video]

    # Create
    class Create:
        upload_test_package = "upload_test_package"
        upload_app = "upload_app"
        create_capability = "create_capability"
        all_create_ability: list = [upload_app, upload_test_package, create_capability]

    # Edit
    class Edit:
        edit_capability = "edit_capability"
        all_edit_ability: list = [edit_capability]

    # Delete
    class Delete:
        delete_test_package = "delete_test_package"
        delete_app = "delete_app"
        delete_capability = "delete_capability"
        all_delete_ability: list = [delete_test_package, delete_app, delete_capability]

    # Execution
    class Execution:
        execute_run = "execute_run"
        download_result = "download_result"
        all_execution_ability: list = [execute_run, download_result]

    all_abilities: list = Read().all_read_ability + \
                          Edit().all_edit_ability + \
                          Create().all_create_ability + \
                          Execution().all_execution_ability + \
                          Delete().all_delete_ability + \
                          Admin().all_admin_ability

    all_normal_user_abilities: list = [Create().upload_app, Delete().delete_app] + Read().default_user_read_ability + Execution().all_execution_ability
