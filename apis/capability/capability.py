from flask import Flask, request, jsonify, Response, current_app
from sqlalchemy import desc
from ability.project_ability import ProjectAbility
from constants.constants import Constants
from status.capability_status import CapabilityStatus
from status.test_package_status import TestPackageStatus
from utils import permission_utils
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity, get_jwt
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from utils import utils
from utils.utils import return_json
from . import capability_blueprint
from ..model.Capability import Capability, db as cap_table
from ..model.TestPackage import TestPackage


def count_scenarios(test_package: TestPackage, feature_file_name, execution_tag):
    total_scenarios = 0
    file_exists = False
    for feature in test_package.features:
        if feature['path'] == feature_file_name:
            file_exists = True
            if execution_tag in feature['count']:
                total_scenarios += feature['count'][execution_tag]
    return file_exists, total_scenarios


@capability_blueprint.route('/getCapabilities', methods=['POST'])
@jwt_required()
def get_capabilities():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_capability):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get capabilities of project id {project_id}, called by {user_id}")
    capabilities = Capability.query.filter_by(project_id=project_id, status=CapabilityStatus.normal).order_by(desc(Capability.created_at)).all()
    capabilities = [c.as_dict() for c in capabilities]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_caps_success,
        {ResponseKey.data: capabilities},
    )


@capability_blueprint.route('/createCapability', methods=['POST'])
@jwt_required()
def create_capability():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.feature_file_name,
        Key.capability_id,
        Key.data_file_name,
        Key.execution_tag,
        Key.test_env,
        Key.name,
        Key.mobile_properties_profile,
        Key.description,
        Key.test_package_id,
        Key.test_package_file_name,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    feature_file_name = posted_json[Key.feature_file_name]
    name = posted_json[Key.name]
    data_file_name = posted_json[Key.data_file_name]
    execution_tag = posted_json[Key.execution_tag]
    test_env = posted_json[Key.test_env]
    mobile_properties_profile = posted_json[Key.mobile_properties_profile]
    capability_id = posted_json[Key.capability_id]
    description = posted_json[Key.description]
    test_package_id = posted_json[Key.test_package_id]
    test_package_file_name = posted_json[Key.test_package_file_name]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Create().create_capability):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    same_name_cap: Capability = Capability.query.filter_by(project_id=project_id, name=name).first()
    if same_name_cap:
        return return_json(
            ResponseStatus.duplicate,
            ResponseValue.cap_name_taken,
        )
    test_package: TestPackage = TestPackage.query.filter_by(test_package_id=test_package_id).first()
    if not test_package:
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    if not str(data_file_name).endswith(Constants.EXCEL_EXTENSION):
        data_file_name += Constants.EXCEL_EXTENSION
    if not str(feature_file_name).endswith(Constants.FEATURE_EXTENSION):
        feature_file_name += Constants.FEATURE_EXTENSION
    if not str(execution_tag).startswith(Constants.TAG_INDICATOR):
        execution_tag = Constants.TAG_INDICATOR + execution_tag
    file_exists, total_scenarios = count_scenarios(test_package, feature_file_name, execution_tag)
    if total_scenarios <= 0:
        if not file_exists:
            return return_json(
                ResponseStatus.no_scenario,
                f"Sorry, but we are unable to find any feature files with the name {feature_file_name}.",
            )
        else:
            return return_json(
                ResponseStatus.no_scenario,
                f"Sorry, but we are unable to find any scenarios with tag {execution_tag} in {feature_file_name} file.",
            )
    current_app.logger.info(f"Create new capability with new cap id of {capability_id}, called by {user_id}")
    new_cap = Capability(
        capability_id=capability_id,
        user_id=user_id,
        name=name,
        project_id=project_id,
        data_file_name=data_file_name,
        feature_file_name=feature_file_name,
        test_env=test_env,
        mobile_properties_profile=mobile_properties_profile,
        execution_tag=execution_tag,
        description=description,
        test_package_id=test_package_id,
        test_package_file_name=test_package_file_name,
        total_scenarios=total_scenarios,
    )
    cap_table.session.add(new_cap)
    cap_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.create_cap_success,
        {ResponseKey.data: new_cap.as_dict()},
    )


@capability_blueprint.route('/editCapability', methods=['POST'])
@jwt_required()
def edit_capability():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.feature_file_name,
        Key.capability_id,
        Key.data_file_name,
        Key.execution_tag,
        Key.test_env,
        Key.name,
        Key.mobile_properties_profile,
        Key.description,
        Key.test_package_id,
        Key.test_package_file_name,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    feature_file_name = posted_json[Key.feature_file_name]
    name = posted_json[Key.name]
    data_file_name = posted_json[Key.data_file_name]
    execution_tag = posted_json[Key.execution_tag]
    test_env = posted_json[Key.test_env]
    mobile_properties_profile = posted_json[Key.mobile_properties_profile]
    capability_id = posted_json[Key.capability_id]
    description = posted_json[Key.description]
    test_package_id = posted_json[Key.test_package_id]
    test_package_file_name = posted_json[Key.test_package_file_name]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Edit().edit_capability):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    same_name_cap: Capability = Capability.query.filter_by(project_id=project_id, name=name).first()
    if same_name_cap and same_name_cap.capability_id != capability_id:
        return return_json(
            ResponseStatus.duplicate,
            ResponseValue.cap_name_taken,
        )
    test_package: TestPackage = TestPackage.query.filter_by(test_package_id=test_package_id).first()
    if not test_package:
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    if not str(data_file_name).endswith(Constants.EXCEL_EXTENSION):
        data_file_name += Constants.EXCEL_EXTENSION
    if not str(feature_file_name).endswith(Constants.FEATURE_EXTENSION):
        feature_file_name += Constants.FEATURE_EXTENSION
    if not str(execution_tag).startswith(Constants.TAG_INDICATOR):
        execution_tag = Constants.TAG_INDICATOR + execution_tag
    file_exists, total_scenarios = count_scenarios(test_package, feature_file_name, execution_tag)
    if total_scenarios <= 0:
        if not file_exists:
            return return_json(
                ResponseStatus.no_scenario,
                f"Sorry, but we are unable to find any feature files with the name {feature_file_name}.",
            )
        else:
            return return_json(
                ResponseStatus.no_scenario,
                f"Sorry, but we are unable to find any scenarios with tag {execution_tag} in {feature_file_name} file.",
            )
    current_app.logger.info(f"Edit capability of cap id of {capability_id}, called by {user_id}")
    target_capability: Capability = Capability.query.filter_by(capability_id=capability_id).first()
    target_capability.feature_file_name = feature_file_name
    target_capability.name = name
    target_capability.data_file_name = data_file_name
    target_capability.execution_tag = execution_tag
    target_capability.test_env = test_env
    target_capability.mobile_properties_profile = mobile_properties_profile
    target_capability.description = description
    target_capability.test_package_id = test_package_id
    target_capability.test_package_file_name = test_package_file_name
    target_capability.total_scenarios = total_scenarios
    cap_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.edit_cap_success,
        {ResponseKey.data: target_capability.as_dict()},
    )


@capability_blueprint.route('/deleteCapabilities', methods=['POST'])
@jwt_required()
def delete_capabilities():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.capability_ids,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    capability_ids = posted_json[Key.capability_ids]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_capability):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    capability_ids = capability_ids.strip('][').split(', ')
    current_app.logger.info(f"Delete {len(capability_ids)} capability, called by {user_id}")
    target_caps = Capability.query.filter(Capability.capability_id.in_(capability_ids)).all()
    for cap in target_caps:
        cap.status = CapabilityStatus.deleted
    cap_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_cap_success,
    )


@capability_blueprint.route('/deleteCapability', methods=['POST'])
@jwt_required()
def delete_capability():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.capability_id,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    capability_id = posted_json[Key.capability_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_capability):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Delete capability of id {capability_id}, called by {user_id}")
    target_capability = Capability.query.filter_by(capability_id=capability_id).first()
    target_capability.status = CapabilityStatus.deleted
    cap_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_cap_success,
    )
