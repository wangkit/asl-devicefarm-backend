from flask import request, current_app
from flask_jwt_extended import current_user
from sqlalchemy import and_, desc, delete, or_

from ability.global_ability import GlobalAbility
from ability.project_ability import ProjectAbility
from status.project_status import ProjectStatus
from status.role_status import RoleStatus
from . import user_blueprint
from flask_jwt_extended import create_access_token, jwt_required
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from status.user_status import UserStatus
from utils import utils, permission_utils
from utils.utils import return_json
from ..model.Project import Project
from ..model.ProjectPermission import ProjectPermission, db as p_table
from ..model.User import User, db as user_table


@user_blueprint.route('/test', methods=['GET'])
def test():
    return "hi"


@user_blueprint.route('/signIn', methods=['POST'])
def sign_in():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.username, Key.password]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request,
        )
    username = posted_json[Key.username]
    password = posted_json[Key.password]
    password = utils.hash_string(current_app.config['SALT'], password)
    user = User.query.filter_by(username=username, password=password, status=UserStatus.normal).one_or_none()
    if user:
        current_app.logger.info(f"Successfully sign in for {user.user_id}")
        user_dict = user.as_dict()
        user_dict.pop('password')
        access_token = create_access_token(identity=user)
        return return_json(
            ResponseStatus.success,
            ResponseValue.sign_in_success,
            {
                ResponseKey.data: user_dict,
                ResponseKey.token: access_token
            }
        )
    else:
        current_app.logger.info(f"Invalid credential from ip {request.remote_addr}")
        current_app.logger.info(f"Invalid username of {username}")
        return return_json(
            ResponseStatus.incorrect_credential,
            ResponseValue.incorrect_credential,
        )


@user_blueprint.route('/changePassword', methods=['POST'])
@jwt_required()
def change_password():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.password,
        Key.user_id,
        Key.new_password,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    current_password = posted_json[Key.password]
    new_password = posted_json[Key.new_password]
    current_password = utils.hash_string(current_app.config['SALT'], current_password)
    new_password = utils.hash_string(current_app.config['SALT'], new_password)
    target_user = User.query.filter_by(user_id=user_id, password=current_password).one_or_none()
    if target_user is None:
        current_app.logger.info(f"Failed to change password for this user id {user_id}")
        return return_json(
            ResponseStatus.incorrect_credential,
            ResponseValue.incorrect_credential,
        )
    target_user.password = new_password
    user_table.session.commit()
    current_app.logger.info(f"Successfully changed password for user id {user_id}")
    return return_json(
        ResponseStatus.success,
        ResponseValue.change_password_success,
    )


@user_blueprint.route('/registerUser', methods=['POST'])
@jwt_required()
def register_normal_user():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.password,
        Key.username,
        Key.user_id,
        Key.project_ids,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    admin_user_id = posted_json[Key.user_id]
    password = posted_json[Key.password]
    username = posted_json[Key.username]
    project_ids = posted_json[Key.project_ids]
    project_ids = project_ids.strip('][').split(', ')
    assigned_project_ids = []
    password = utils.hash_string(current_app.config['SALT'], password)
    description = None
    if Key.description in posted_json:
        description = posted_json[Key.description]
    if current_user.type != RoleStatus.admin:
        current_app.logger.info(f"Unauthorized attempt to create user, called by {current_user.user_id}")
        return return_json(
            ResponseStatus.forbidden,
            f"You are not authorized to create a new user.",
        )
    if User.query.filter_by(username=username).one_or_none():
        current_app.logger.info(f"Duplicated username of {username}")
        return return_json(
            ResponseStatus.duplicate,
            f"{username} has already been taken. Please use another name.",
        )
    user_id = utils.get_uuid()
    new_user = User(
        user_id=user_id,
        username=username,
        password=password,
        status=UserStatus.normal,
        description=description,
        permissions=GlobalAbility.none,
        type=RoleStatus.normal,
        upstream_admin_id=admin_user_id,
    )

    # Insert new normal user
    user_table.session.add(new_user)
    user_table.session.commit()
    current_app.logger.info(f"Successfully registered a new user account with username {username}")

    # Assign this new user with permissions to access the projects
    for project_id in project_ids:
        if not permission_utils.has_permission(admin_user_id, project_id, ProjectAbility.Admin().assign_user_to_project):
            current_app.logger.info(f"Admin has no right to assign a new user {username} to project {project_id}")
            continue
        if utils.is_valid_uuid(project_id):
            assigned_project_ids.append(project_id)
            for ability in ProjectAbility.all_normal_user_abilities:
                p = ProjectPermission(
                    permission_id=utils.get_uuid(),
                    user_id=user_id,
                    project_id=project_id,
                    ability=ability,
                )
                p_table.session.add(p)
    p_table.session.commit()
    current_app.logger.info(f"Successfully assigned user with username {username} normal user abilities")

    # Fetch the new user and return it
    new_user = User.query.filter_by(user_id=user_id).one_or_none()
    new_user_dict = new_user.as_dict()
    new_user_dict.pop('password')
    return return_json(
        ResponseStatus.success,
        f"User {username} has been created successfully.",
        {
            ResponseKey.data: {
                ResponseKey.new_user: new_user_dict,
                ResponseKey.assigned_projects: assigned_project_ids,
            },
        }
    )


@user_blueprint.route('/registerAdmin', methods=['POST'])
def register_admin():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.password,
        Key.username,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    password = posted_json[Key.password]
    username = posted_json[Key.username]
    password = utils.hash_string(current_app.config['SALT'], password)
    description = None
    if Key.description in posted_json:
        description = posted_json[Key.description]
    user_with_username = User.query.filter_by(username=username).one_or_none()
    if user_with_username:
        current_app.logger.info(f"Duplicated username of {username}")
        return return_json(
            ResponseStatus.duplicate,
            ResponseValue.duplicate_username,
        )
    user_id = utils.get_uuid()
    new_user = User(
        user_id=user_id,
        username=username,
        password=password,
        status=UserStatus.normal,
        description=description,
        permissions=GlobalAbility.all_abilities,
        type=RoleStatus.admin,
    )

    # Insert new admin user
    user_table.session.add(new_user)
    user_table.session.commit()
    current_app.logger.info(f"Successfully registered a new admin account for username {username}")

    # Fetch the new user and return it
    new_user = User.query.filter_by(user_id=user_id).one_or_none()
    new_user_dict = new_user.as_dict()
    new_user_dict.pop('password')
    access_token = create_access_token(identity=new_user)
    return return_json(
        ResponseStatus.success,
        ResponseValue.admin_account_created_success,
        {
            ResponseKey.data: new_user_dict,
            ResponseKey.token: access_token
        }
    )


@user_blueprint.route('/removeUsersFromProject', methods=['POST'])
@jwt_required()
def remove_users_from_project():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id, Key.user_ids]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    normal_user_ids = posted_json[Key.user_ids]
    if current_user.user_id != user_id or current_user.type != RoleStatus.admin:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Admin().remove_user_from_project):
        current_app.logger.info(f"{user_id} has no right to remove user from project {project_id}")
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    normal_user_ids = normal_user_ids.strip('][').split(', ')
    current_app.logger.info(f"Removing {len(normal_user_ids)} users to {project_id}")
    # Assign this user with permissions to access the project
    for normal_user_id in normal_user_ids:
        if utils.is_valid_uuid(normal_user_id):
            del_stmt = delete(ProjectPermission).where(and_(ProjectPermission.user_id == normal_user_id, ProjectPermission.project_id == project_id))
            p_table.session.execute(del_stmt)
    p_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.remove_user_success,
    )


@user_blueprint.route('/assignUsersToProject', methods=['POST'])
@jwt_required()
def assign_users_to_project():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id, Key.user_ids]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    normal_user_ids = posted_json[Key.user_ids]
    if current_user.user_id != user_id or current_user.type != RoleStatus.admin:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Admin().assign_user_to_project):
        current_app.logger.info(f"{user_id} has no right to assign user to project {project_id}")
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    normal_user_ids = normal_user_ids.strip('][').split(', ')
    current_app.logger.info(f"Assigning {len(normal_user_ids)} users to {project_id}")
    # Assign this user with permissions to access the project
    for normal_user_id in normal_user_ids:
        if utils.is_valid_uuid(project_id):
            for ability in ProjectAbility.all_normal_user_abilities:
                p = ProjectPermission(
                    permission_id=utils.get_uuid(),
                    user_id=normal_user_id,
                    project_id=project_id,
                    ability=ability,
                )
                p_table.session.add(p)
    p_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.assign_user_success,
    )


@user_blueprint.route('/getAssignUsers', methods=['POST'])
@jwt_required()
def get_assign_to_project_users():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    if current_user.user_id != user_id or current_user.type != RoleStatus.admin:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Admin().get_assign_users):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get normal users who can be assigned to project {project_id} of admin {user_id}")
    users_already_in_this_project = p_table.session.query(ProjectPermission.user_id).filter(and_(ProjectPermission.project_id == project_id))
    users = User.query.filter(and_(or_(User.upstream_admin_id == user_id, User.user_id == user_id), User.user_id.not_in(users_already_in_this_project))) \
        .order_by(desc(User.created_at)).all()
    user_dict = [u.as_dict() for u in users]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_assign_user_success,
        {ResponseKey.data: user_dict}
    )


@user_blueprint.route('/getUsers', methods=['POST'])
@jwt_required()
def get_users():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    if current_user.user_id != user_id or current_user.type != RoleStatus.admin:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get downstream users of {user_id}")
    # Get all downstream without considering if the normal user is in status other than normal
    users = User.query.filter_by(upstream_admin_id=user_id).order_by(desc(User.created_at)).all()
    user_dict = [u.as_dict() for u in users]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_user_success,
        {ResponseKey.data: user_dict}
    )


@user_blueprint.route('/getMyProfile', methods=['POST'])
@jwt_required()
def get_my_profile():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    if current_user.user_id != user_id:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden
        )
    user = User.query.filter_by(user_id=user_id).one_or_none()
    user_dict = user.as_dict()
    user_dict.pop('password')
    current_app.logger.info(f"Get my profile for user id {user.user_id}")
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_my_profile_success,
        {ResponseKey.data: user_dict}
    )


@user_blueprint.route('/getProjectUsers', methods=['POST'])
@jwt_required()
def get_project_users():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Admin().get_project_user):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get projects users of project {project_id}")
    users = User.query.join(ProjectPermission, ProjectPermission.project_id == project_id) \
        .filter(and_(User.user_id != user_id, ProjectPermission.user_id == User.user_id, User.status == UserStatus.normal)) \
        .order_by(desc(User.created_at)).all()
    user_dict = [u.as_dict() for u in users]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_project_user_success,
        {ResponseKey.data: user_dict}
    )