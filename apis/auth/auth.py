from key.key import Key
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from utils import utils
from utils.utils import return_json
from . import auth_blueprint
import json
from flask import Flask, request, jsonify, Response, current_app
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity, get_jwt, current_user
import datetime
from response.response_key import ResponseKey
from .. import User


@auth_blueprint.after_request
def refresh_expiring_jwts(response: Response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.datetime.now(datetime.timezone.utc)
        target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(minutes=20))
        if target_timestamp > exp_timestamp:
            access_token = create_access_token(identity=get_jwt_identity())
            d = response.get_json()
            d[ResponseKey.token] = access_token
            response.data = json.dumps(d)
            current_app.logger.info("Implicitly refreshed access token after key")
        return response
    except (RuntimeError, KeyError):
        current_app.logger.info("Failed to Implicitly refresh access token after key")
        return response


@auth_blueprint.route('/refresh', methods=['POST'])
def refresh():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    user = User.query.filter_by(user_id=user_id).one_or_none()
    if not user:
        current_app.logger.info(f"Failed to find the user: {user_id}")
        return return_json(
            ResponseStatus.no_such_user,
            ResponseValue.no_such_user,
        )
    current_app.logger.info("Explicitly refreshed access token")
    return return_json(
        ResponseStatus.success,
        ResponseValue.refresh_success,
        {ResponseKey.token: create_access_token(identity=user)}
    )