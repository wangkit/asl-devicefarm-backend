import datetime

from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from flask_sqlalchemy import SQLAlchemy
from status.job_status import JobStatus

db = SQLAlchemy()


class ExecutionQueue(db.Model):
    __tablename__ = 'execution_queue'
    job_id = Column(Text, nullable=False, primary_key=True)
    batch_id = Column(Text, nullable=False)
    execution_id = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    user_id = Column(Text, nullable=False)
    status = Column(Integer, default=JobStatus.pending)
    retry_time = Column(Integer, default=0)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    next_run_time = Column(DateTime, nullable=True)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
