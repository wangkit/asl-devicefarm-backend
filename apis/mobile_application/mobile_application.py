from eventlet.green import subprocess
import sys
from sqlalchemy import desc, and_, or_
from flask import Flask, request, jsonify, Response, current_app
from werkzeug.utils import secure_filename
import os
from ability.project_ability import ProjectAbility
from download import download
from status.app_status import AppStatus
from status.capability_status import CapabilityStatus
from status.project_status import ProjectStatus
from status.test_package_status import TestPackageStatus
from utils import permission_utils
import string
import random
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity, get_jwt
import datetime
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from status.user_status import UserStatus
from utils import utils
from constants.constants import Constants
from utils.utils import return_json
from . import mobile_application_blueprint
from .. import conn
from ..model.Application import Application, db as app_table


@mobile_application_blueprint.route('/uploadApp', methods=['POST'])
@jwt_required()
def create_upload_app():
    if Key.app_file not in request.files:
        return return_json(
            ResponseStatus.empty_file,
            ResponseValue.empty_file
        )
    if Key.project_id not in request.form:
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    app_file = request.files[Key.app_file]
    project_id = request.form[Key.project_id]
    user_id = request.form[Key.user_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Create().upload_app):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if not app_file.filename:
        current_app.logger.info(f"Create upload app without a filename {app_file.filename}, requested by user id {user_id}")
        return return_json(
            ResponseStatus.empty_file,
            ResponseValue.empty_file
        )
    if app_file and utils.allowed_file(app_file.filename):
        app_filename = secure_filename(app_file.filename)
        app_file_path = os.path.join(download.file_folder, app_filename)
        app_file.save(app_file_path)
        app_id = utils.get_uuid()
        upload_id = utils.get_uuid()
        app_size = utils.get_file_size(app_file_path)
        if app_filename.endswith(".ipa"):
            app_type = Constants.IOS_APP
            app_name, app_identifier, app_version = utils.get_ipa_details(app_file_path)
        else:
            app_type = Constants.ANDROID_APP
            app_name, app_identifier, app_version = utils.get_apk_details(app_file_path)
        current_app.logger.info(f"New {app_type} file with the name of {app_filename} received, app identifier is {app_identifier}, app version is {app_version}")
        uploaded_s3_file_path = utils.get_s3_upload_path(project_id, app_type, upload_id, app_filename)
        current_app.logger.info(f"Begin uploading app {app_file_path} to {Constants.DEFAULT_BUCKET} as {uploaded_s3_file_path}")
        subprocess.Popen([sys.executable, './upload_to_s3.py', app_file_path, Constants.DEFAULT_BUCKET, uploaded_s3_file_path,
                          "update app_files set status = {status} where app_id = '{app_id}'".format(status=AppStatus.normal, app_id=app_id),
                          current_app.config['SCRIPT_SECRET_KEY'], project_id, "app_status_update", app_id])
        current_app.logger.info(f"Successfully started upload app command")
        new_app = Application(
            app_id=app_id,
            upload_id=upload_id,
            project_id=project_id,
            file_name=app_filename,
            app_version=app_version,
            app_name=app_name,
            app_identifier=app_identifier,
            kind=app_type,
            user_id=user_id,
            file_path=uploaded_s3_file_path,
            file_size=app_size,
            status=TestPackageStatus.uploading,
        )
        app_table.session.add(new_app)
        app_table.session.commit()
        inserted_app = Application.query.filter_by(app_id=app_id).one_or_none()
        return return_json(
            ResponseStatus.success,
            ResponseValue.upload_app_success,
            {ResponseKey.data: inserted_app.as_dict()},
        )
    return return_json(
        ResponseStatus.bad_request,
        ResponseValue.malformed_request,
    )


@mobile_application_blueprint.route('/getApp', methods=['POST'])
@jwt_required()
def get_application():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id, Key.app_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    app_id = posted_json[Key.app_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_application):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get one application of {app_id} of project id {project_id}, requested by {user_id}")
    app = Application.query.filter(and_(Application.project_id == project_id, Application.app_id == app_id)).one_or_none()
    if app:
        app = app.as_dict()
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_apps_success,
        {ResponseKey.data: app},
    )


@mobile_application_blueprint.route('/getApps', methods=['POST'])
@jwt_required()
def get_applications():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id, Key.offset]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    offset = posted_json[Key.offset]
    kind = ""
    if Key.kind in posted_json:
        kind = posted_json[Key.kind]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_application):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Get applications of project id {project_id}, requested by {user_id}")
    applications = Application.query.filter(and_(Application.project_id == project_id, or_(Application.status == AppStatus.normal, Application.status == AppStatus.uploading))).order_by(desc(Application.created_at)).all()
    applications = [a.as_dict() for a in applications]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_apps_success,
        {ResponseKey.data: applications},
    )


@mobile_application_blueprint.route('/deleteApplications', methods=['POST'])
@jwt_required()
def delete_applications():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.app_ids,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    app_ids = posted_json[Key.app_ids]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_app):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    app_ids = app_ids.strip('][').split(', ')
    current_app.logger.info(f"Delete {len(app_ids)} applications, requested by {user_id}")
    target_apps = Application.query.filter(Application.app_id.in_(app_ids)).all()
    for app in target_apps:
        app.status = AppStatus.deleted
    app_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_apps_success,
    )


@mobile_application_blueprint.route('/deleteApplication', methods=['POST'])
@jwt_required()
def delete_application():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [
        Key.user_id,
        Key.project_id,
        Key.app_id,
    ]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    app_id = posted_json[Key.app_id]
    if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Delete().delete_app):
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    current_app.logger.info(f"Delete application of app id {app_id}, requested by {user_id}")
    target_app = Application.query.filter_by(app_id=app_id).first()
    target_app.status = AppStatus.deleted
    app_table.session.commit()
    return return_json(
        ResponseStatus.success,
        ResponseValue.delete_app_success,
    )
