import os

"""
Save uploaded and downloaded file to this path
The path that the uploaded and downloaded files will go to is the folder that this download.py resides in
"""
file_folder = os.path.dirname(os.path.realpath(__file__))