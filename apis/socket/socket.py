from .. import socketio_manager
from key.key import Key
from flask_socketio import emit, join_room, leave_room
from utils.log_utils import get_logger


logger = get_logger("flask-socket-server")


@socketio_manager.on_error_default
def default_error_handler(e):
    logger.critical(f"Socket server error: {e}")


@socketio_manager.on('run_status_update_router', namespace='/')
def run_status_update_router(data):
    logger.info(f"Run status router called with data {data}")
    if Key.project_id in data and Key.id in data:
        logger.info(f"Emit to run_status_update")
        emit("run_status_update",
             {Key.execution_id: data[Key.id]},
             room=data[Key.project_id])


@socketio_manager.on('test_package_status_update_router', namespace='/')
def test_package_status_update_router(data):
    logger.info(f"Test package status router called with data {data}")
    if Key.project_id in data and Key.id in data:
        logger.info(f"Emit to test_package_upload_finished")
        emit("test_package_upload_finished",
             {Key.test_package_id: data[Key.id]},
             room=data[Key.project_id])


@socketio_manager.on('app_status_update_router', namespace='/')
def app_status_update_router(data):
    logger.info(f"App status router called with data {data}")
    if Key.project_id in data and Key.id in data:
        logger.info(f"Emit to app_upload_finished")
        emit("app_upload_finished",
             {Key.app_id: data[Key.id]},
             broadcast=True)


@socketio_manager.on('join_project', namespace='/')
def join_project(data):
    logger.info(f"Join project event triggered with data {data}")
    if Key.project_id in data:
        project_id = data[Key.project_id]
        logger.info(f"Joining room with project id of {project_id}")
        join_room(project_id)


@socketio_manager.on('leave_project', namespace='/')
def leave_project(data):
    logger.info(f"Leave project event triggered with data {data}")
    if Key.project_id in data:
        project_id = data[Key.project_id]
        logger.info(f"Leaving room with project id of {project_id}")
        leave_room(project_id)
