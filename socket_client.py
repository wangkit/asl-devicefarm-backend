"""
########################################################################################
This python script is created to accomplish the goal to update the frontend of the
status of upload app and upload test package and schedule run in real time. The more
convenient and logical way to emit event to the frontend from the upload_to_s3 script or
the run_maven.py is deemed impossible at the date. An update of eventlet, Flask, socketio
might help. So it might be useful to reimplement the more logical way in a later time.
As of writing, the eventlet version is
eventlet==0.30.2
########################################################################################
"""
import time
import socketio
from apis.config.config import SCRIPT_SECRET_KEY
from key.key import Key
from utils.log_utils import get_logger
import sys

logger = get_logger("flask-socket-client")

if len(sys.argv) != 5:
    logger.warning(f"-------------------EXITING SOCKETIO CLIENT SCRIPT-------------------")
    logger.warning(f"The length of arguments received is not valid. Expected 5, but got {len(sys.argv)}")
    logger.warning(f"Received arguments {sys.argv}")
    logger.warning(f"-----------------------------------------------------------------")
    sys.exit("Incoming arguments are invalid")

# The event name to emit name
socket_event_name = sys.argv[1]

# The room id to pass on to the server
project_id = sys.argv[2]

# The secret key for auth
secret = sys.argv[3]

# The object id. This is either app id or test package id
object_id = sys.argv[4]

if not socket_event_name or not project_id:
    logger.warning(f"-------------------EXITING SOCKETIO CLIENT SCRIPT-------------------")
    logger.warning(f"Received arguments are invalid: {sys.argv}")
    logger.warning(f"--------------------------------------------------------------------")
    sys.exit("Incoming arguments are invalid")

if secret != SCRIPT_SECRET_KEY:
    logger.warning(f"-------------------EXITING SOCKETIO CLIENT SCRIPT-------------------")
    logger.warning(f"Received secret key {secret} is invalid")
    logger.warning(f"--------------------------------------------------------------------")
    sys.exit("Script secret key is invalid")


sio = socketio.Client()
try:
    sio.connect('http://localhost:8000', transports=["websocket"])
    logger.info("-----------CONNECTED TO SERVER-----------")

    socket_event_name_router = socket_event_name + "_router"
    logger.info(f"Begin emitting with id {project_id} and event {socket_event_name_router}")
    sio.emit(socket_event_name_router, {
        Key.project_id: project_id,
        Key.id: object_id,
    })

    # We could have used callback in emit function to perform disconnect
    # However we find that the callback sometimes is not triggered
    # thus keeping the connection alive for too long. It is safer to
    # assume the emit will be done in a few seconds and drop the
    # connection manually.
    t = 60
    logger.info(f"-----------WAIT {t} BEGINS-----------")
    time.sleep(t)
except Exception as e:
    logger.critical(f"Socket client error occurred: {e}")
finally:
    sio.disconnect()
    logger.info("-----------DISCONNECTED FROM SERVER-----------")
    sys.exit("Exit socket client script")
