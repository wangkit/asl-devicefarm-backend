class ResponseStatus:
    """
    The status code returned to frontend
    """
    success = 200
    incorrect_credential = 201
    duplicate = 202
    no_data = 203
    fail_to_create = 205
    wrong_data = 206
    insufficient_quota = 207
    bad_request = 400
    forbidden = 403
    server_error = 407
    empty_file = 508
    no_such_user = 510
    no_scenario = 537
