import logging
import os
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler


def get_logger(logger_name: str):
    # Create a file handler object
    file_handler = get_timed_log_file(logger_name)

    # Create a new logger
    logger = logging.getLogger(logger_name)

    # Set the logging level of the file handler object so that it logs DEBUG and up
    logger.setLevel(logging.DEBUG)

    # Apply the file formatter object to the file handler object
    file_handler.setFormatter(get_logger_format())

    # Add file handler object to the logger
    logger.addHandler(file_handler)

    return logger


def get_timed_log_file(name: str):
    if not os.path.exists("logs"):
        os.mkdir("logs")
    return TimedRotatingFileHandler(f'logs/{str(datetime.now().date()).replace("-", "")}-{name}.log', when="D",
                                    interval=1, backupCount=15,
                                    encoding="UTF-8", delay=False, utc=True)


def get_logger_format():
    return logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(filename)s: %(lineno)d]')
