from flask import current_app, request
from apis.model.ProjectPermission import ProjectPermission


def has_permission(user_id, project_id, required_ability):
    p_permission = ProjectPermission.query.filter_by(
        user_id=user_id,
        project_id=project_id,
        ability=required_ability
    ).one_or_none()
    if p_permission:
        current_app.logger.info(f"User id {user_id} have ability {required_ability} for project {project_id}. The path of the request is {request.path}.")
        return True
    current_app.logger.warning(f"FORBIDDEN: User id {user_id} has no right to do {required_ability} on project of project id {project_id}. The path of the request is {request.path}.")
    return False

