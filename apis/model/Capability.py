import datetime

from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from flask_sqlalchemy import SQLAlchemy
from status.capability_status import CapabilityStatus


db = SQLAlchemy()


class Capability(db.Model):
    __tablename__ = 'capabilities'
    capability_id = Column(Text, primary_key=True, nullable=False)
    description = Column(Text, nullable=False)
    user_id = Column(Text, nullable=False)
    name = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    test_package_id = Column(Text, nullable=False)
    test_package_file_name = Column(Text, nullable=False)
    data_file_name = Column(Text, nullable=False)
    feature_file_name = Column(Text, nullable=False)
    test_env = Column(Text, nullable=True)
    mobile_properties_profile = Column(Text, nullable=True)
    total_scenarios = Column(Integer, nullable=False)
    execution_tag = Column(Text, nullable=True)
    status = Column(Integer, nullable=False, default=CapabilityStatus.normal)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
