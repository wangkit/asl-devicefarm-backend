"""
########################################################################################
This python script is to support running the maven command that triggers SauceLab
execution separately from the application.py to avoid blocking the RESTFul service.
########################################################################################
"""

import os
import requests
from apis.config.config import SCRIPT_SECRET_KEY, SAUCELAB_USERNAME, SAUCELAB_ACCESS_KEY, SAUCELAB_REGION
from constants.constants import Constants
from key.key import Key
import subprocess
import sys
from utils import utils
from utils.log_utils import get_logger

logger = get_logger("schedule-run")

if len(sys.argv) != 15:
    logger.warning(f"-------------------EXITING SCHEDULE RUN SCRIPT-------------------")
    logger.warning(f"The length of arguments received is not valid. Expected 13, but got {len(sys.argv)}")
    logger.warning(f"Received arguments {sys.argv}")
    logger.warning(f"-----------------------------------------------------------------")
    sys.exit("Incoming arguments are invalid")

# The execution id of this run
execution_id = sys.argv[1]

# The local file path to the test package file
test_package_file_path = sys.argv[2]

# The local file name of the test package file
test_package_file_name = sys.argv[3]

# The test environment
test_env = sys.argv[4]

# The feature file name to be used
feature_file_name = sys.argv[5]

# The data file name to be used
data_file_name = sys.argv[6]

# The execution tag of the target scenarios
execution_tag = sys.argv[7]

# The mobile property used to send to ACT
mobile_properties_profile = sys.argv[8]

# The target device id
device_id = sys.argv[9]

# The platform of the target device
app_kind = sys.argv[10]

# The platform version of the target device
platform_version = sys.argv[11]

# The link to the app file
app_file_url = sys.argv[12]

# The secret key
secret = sys.argv[13]

# Job id
job_id = sys.argv[14]

if secret != SCRIPT_SECRET_KEY:
    logger.warning(f"-------------------EXITING SCHEDULE RUN SCRIPT-------------------")
    logger.warning(f"Received secret key {secret} is invalid")
    logger.warning(f"-----------------------------------------------------------------")
    sys.exit("Script secret key is invalid")

logger.info(f"--------------Beginning execution--------------")
logger.info(f"Execution id: {execution_id}")
logger.info(f"Test package file path: {test_package_file_path}")
logger.info(f"Test package file name: {test_package_file_name}")
logger.info(f"Test env: {test_env}")
logger.info(f"Feature file name: {feature_file_name}")
logger.info(f"Data file name: {data_file_name}")
logger.info(f"Execution tag: {execution_tag}")
logger.info(f"Mobile properties profile: {mobile_properties_profile}")
logger.info(f"Device id: {device_id}")
logger.info(f"App kind: {app_kind}")
logger.info(f"Platform version: {platform_version}")
logger.info(f"App file url: {app_file_url}")
logger.info(f"-----------------------------------------------")

try:
    # Set the target directory to download file
    target_directory = utils.create_download_folder(execution_id)

    logger.info(f"Target directory of the downloaded file: {target_directory}")

    # Download target test package from s3
    utils.download_from_s3(test_package_file_path, Constants.DEFAULT_BUCKET,
                           os.path.join(target_directory, test_package_file_name))

    # Unzip the downloaded zip file
    logger.info(f"Unzipping downloaded file: {test_package_file_name}")
    unzip_process = subprocess.Popen(['unzip', '-o', test_package_file_name], cwd=target_directory)
    unzip_process.wait()

    # Run command to trigger automation on SauceLab
    run_cmd = "mvn test -Dappium.remote.address=https://ondemand.%s.saucelabs.com/wd/hub -Dtest.name=%s -Dtest.env=%s -DfeatureFileName=%s -DdataFileName=%s -DexecutionTag=%s -Dvideo.capture=y -Dmobile.properties.profile=%s -Dsaucelab.username=%s -Dsaucelab.access.key=%s -Ddevice.name=%s -Dplatform.name=%s -Dplatform.version=%s -Dmobile.capability.app=%s -Dexecution.id=%s > %s"
    run_cmd = run_cmd % (
        SAUCELAB_REGION, execution_id, test_env, feature_file_name, data_file_name, execution_tag, mobile_properties_profile, SAUCELAB_USERNAME,
        SAUCELAB_ACCESS_KEY, device_id, app_kind, platform_version, app_file_url, execution_id, Constants.LOG_FILE,)
    logger.info(f"Run command to trigger automation")
    logger.info(f"-----------------COMMAND-----------------")
    logger.info(run_cmd)
    logger.info(f"-----------------------------------------")
    process = subprocess.Popen(run_cmd, cwd=target_directory, shell=True)
    logger.info(f"Waiting for execution to finish")
    process.wait()
    logger.info(f"Execution ends")
finally:
    # Call the endTest endpoint to acknowledge the end of test and begin collecting results in output folder
    logger.info(f"Calling localhost's endTest endpoint")
    r = requests.post('http://localhost:8000/api/endTest', json={
        Key.execution_id: execution_id,
        Key.job_id: job_id,
    })
    logger.info(f"Call endTest result {r.json()}")

