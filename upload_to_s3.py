"""
########################################################################################
This python script is created to support separating the process of uploading to s3
from the main Flask application process in order to allow faster response time.
########################################################################################
"""

from eventlet.green import subprocess
import os
from apis.config.config import s3_client, conn, SCRIPT_SECRET_KEY
import sys
from utils.log_utils import get_logger
from utils.utils import get_cursor


logger = get_logger('upload-to-s3')

try:
    if len(sys.argv) != 9:
        logger.warning("-------------------EXITING UPLOAD TO S3 SCRIPT-------------------")
        logger.warning(f"The length of arguments received is not valid. Expected 9, but got {len(sys.argv)}")
        logger.warning(f"Received arguments {sys.argv}")
        logger.warning("-----------------------------------------------------------------")
        sys.exit("Incoming arguments are invalid")

    # The path of the file that will be uploaded
    local_file_path = sys.argv[1]

    # The bucket on s3 that the file will be uploaded to
    bucket_name = sys.argv[2]

    # The path of the uploaded object on s3 in the bucket
    remote_uploaded_file_path = sys.argv[3]

    # The SQL command that needs to be run after uploading
    sql_statement = sys.argv[4]

    # The secret key
    secret = sys.argv[5]

    # The project id of the project that this file belongs to
    project_id = sys.argv[6]

    # The socket event that this upload should trigger
    socket_event_name = sys.argv[7]

    # The objects' id to pass back to the frontend through socket
    object_id = sys.argv[8]

    if secret != SCRIPT_SECRET_KEY:
        logger.warning("-------------------EXITING UPLOAD TO S3 SCRIPT-------------------")
        logger.warning(f"Received secret key {secret} is invalid")
        logger.warning("-----------------------------------------------------------------")
        sys.exit("Script secret key is invalid")

    logger.info(f"--------------BEGIN UPLOADING------------------")
    logger.info(f"From local file path: {local_file_path}")
    logger.info(f"To target bucket name: {bucket_name}")
    logger.info(f"As remote uploaded file: {remote_uploaded_file_path}")
    logger.info(f"-----------------------------------------------")
    # We cannot allow the files uploaded in this file to be private unless we can find a way to allow
    # saucelab to read private app files and the backend to read private test packages
    s3_client.upload_file(local_file_path, bucket_name, remote_uploaded_file_path, ExtraArgs={'ACL': 'public-read'})
    logger.info(f"Finished uploading file at {local_file_path}")
    logger.info(f"Removing the uploaded file from {local_file_path}.")
    os.remove(local_file_path)
    logger.info(f"Removed file at {local_file_path}")

    # If the sql statement received is not empty or None, execute it
    if sql_statement:
        cursor = get_cursor()
        logger.info(f"Begin executing statement {sql_statement}")
        cursor.execute(sql_statement)
        conn.commit()
        cursor.close()
        logger.info(f"Successfully executed statement")
        logger.info(f"Begin calling socketio client script for event {socket_event_name} with room id {project_id}")
        subprocess.Popen([sys.executable, './socket_client.py', socket_event_name, project_id, SCRIPT_SECRET_KEY, object_id])
        logger.info(f"Successfully called")
except Exception as e:
    conn.rollback()
    logger.warning(f"Encountered error while uploading file. The error is {e}")
