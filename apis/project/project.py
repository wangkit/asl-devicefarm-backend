from flask import request, current_app
from sqlalchemy import desc, and_

from ability.global_ability import GlobalAbility
from ability.project_ability import ProjectAbility
from status.project_status import ProjectStatus
from flask_jwt_extended import jwt_required, current_user
from key.key import Key
from response.response_key import ResponseKey
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from status.role_status import RoleStatus
from status.user_status import UserStatus
from utils import utils, permission_utils
from utils.utils import return_json
from . import project_blueprint
from .. import conn, User
from ..model.Project import Project, db as project_table
from ..model.ProjectPermission import db as project_permission_table
from ..model.ProjectPermission import ProjectPermission


@project_blueprint.route('/getLatestProject', methods=['POST'])
@jwt_required()
def get_latest_project():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id, Key.project_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    project_id = posted_json[Key.project_id]
    latest_project = None
    if project_id:
        if not permission_utils.has_permission(user_id, project_id, ProjectAbility.Read().read_project):
            return return_json(
                ResponseStatus.forbidden,
                ResponseValue.forbidden,
            )
        latest_project = Project.query.filter_by(project_id=project_id, status=ProjectStatus.normal).limit(1).one_or_none()
        current_app.logger.info(f"Get latest project of project id {project_id}, requested by {user_id}")
    else:
        latest_project = Project.query.join(ProjectPermission, Project.project_id == ProjectPermission.project_id) \
            .filter(and_(ProjectPermission.user_id == user_id, ProjectPermission.ability == ProjectAbility.Read().read_project,
                         Project.status == ProjectStatus.normal)).order_by(desc(Project.created_at)).limit(1).one_or_none()
        current_app.logger.info(f"Get latest project of user id {user_id}")
    if latest_project is None or not latest_project:
        r = {}
    else:
        r = {ResponseKey.data: latest_project.as_dict()}
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_latest_project_success,
        r,
    )


@project_blueprint.route('/getProjects', methods=['POST'])
@jwt_required()
def get_projects():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.user_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    user_id = posted_json[Key.user_id]
    current_app.logger.info(f"Get projects of user id {user_id}")
    projects = Project.query.join(ProjectPermission, Project.project_id == ProjectPermission.project_id) \
        .filter(and_(ProjectPermission.user_id == user_id, ProjectPermission.ability == ProjectAbility.Read().read_project,
                     Project.status == ProjectStatus.normal)).order_by(desc(Project.created_at)).all()
    projects_list_with_dict = [p.as_dict() for p in projects]
    return return_json(
        ResponseStatus.success,
        ResponseValue.get_projects_success,
        {ResponseKey.data: projects_list_with_dict},
    )


@project_blueprint.route('/createProject', methods=['POST'])
@jwt_required()
def create_project():
    posted_json = request.get_json()
    if not posted_json or utils.confirm_data_exist(posted_json, [Key.name, Key.user_id]):
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    project_name = posted_json[Key.name]
    user_id = posted_json[Key.user_id]
    if GlobalAbility.Create().create_project not in current_user.permissions:
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    project_id = utils.get_uuid()
    new_project = Project(
        project_id=project_id,
        user_id=user_id,
        project_name=project_name,
    )

    # Insert new project
    project_table.session.add(new_project)
    project_table.session.commit()

    # Give all permissions to this project to the creator
    for ability in ProjectAbility.all_abilities:
        permission_id = utils.get_uuid()
        project_permission_table.session.add(ProjectPermission(
            permission_id=permission_id,
            user_id=user_id,
            project_id=project_id,
            ability=ability
        ))
        project_permission_table.session.commit()

    # Fetch the newly inserted project
    inserted_project = Project.query.filter_by(project_id=project_id, status=ProjectStatus.normal).one_or_none()
    return return_json(
        ResponseStatus.success,
        ResponseValue.create_project_success,
        {ResponseKey.data: inserted_project.as_dict()},
    )
