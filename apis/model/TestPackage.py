import datetime
from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from flask_sqlalchemy import SQLAlchemy
from status.test_package_status import TestPackageStatus


db = SQLAlchemy()


class TestPackage(db.Model):
    __tablename__ = 'test_packages'
    test_package_id = Column(Text, primary_key=True, nullable=False)
    upload_id = Column(Text, nullable=False)
    project_id = Column(Text, nullable=False)
    file_name = Column(Text, nullable=False)
    description = Column(Text, nullable=False)
    user_id = Column(Text, nullable=False)
    file_path = Column(Text, nullable=False)
    file_size = Column(Text, nullable=True)
    features = Column(JSON, nullable=False)
    status = Column(Integer, nullable=False, default=TestPackageStatus.normal)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
