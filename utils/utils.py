import datetime
import hashlib
import logging
import plistlib
import random
import re
import string
import time
import uuid
import zipfile
import psycopg2
from botocore.exceptions import ClientError
from flask import jsonify, request, current_app
from psycopg2.extras import RealDictCursor
from pyaxmlparser import APK
import os
from flask import request as flask_request
from apis.config.config import conn, s3_client, SALT, AWS_REGION
from constants.constants import Constants
from download import download
from response.response_key import ResponseKey
from response.response_status import ResponseStatus


def is_valid_uuid(val):
    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False


def get_uuid():
    return str(uuid.uuid4())


def hash_string(salt, hash_target):
    """
    :param salt: The salt
    :param hash_target: The password
    :return:
    """
    hash_me = hashlib.sha512()
    hash_me.update(('%s%s' % (salt, hash_target)).encode('utf-8'))
    hashed_target = hash_me.hexdigest()
    return hashed_target


def get_output_folder(execution_id):
    return os.path.join(download.file_folder, execution_id, Constants.OUTPUT)


def create_download_folder(execution_id):
    try:
        d = os.path.join(download.file_folder, execution_id)
        os.makedirs(d)
        return d
    except Exception as e:
        return os.path.join(download.file_folder, execution_id)


def get_download_folder(execution_id):
    try:
        return os.path.join(download.file_folder, execution_id)
    except Exception as e:
        return os.path.join(download.file_folder, execution_id)


def return_json(status: ResponseStatus, value: any, data=None):
    if data is None:
        data = {}
    return_data = {
        ResponseKey.description: value,
    }
    return_data.update(data)
    return jsonify(return_data), status


def get_cursor(isDict=True):
    try:
        if isDict:
            return conn.cursor(cursor_factory=RealDictCursor)
        else:
            return conn.cursor()
    except psycopg2.InterfaceError:
        print("Connection already closed")
        return None


def confirm_data_exist(json, required_keys: list):
    for key in required_keys:
        if key not in json:
            # Return true when required key doesn't exist
            current_app.logger.info(f"Required key of {key} not found. The request is sent to {request.path}.")
            return True
    return False


def get_unique_name():
    return "Run-" + (datetime.date.today().isoformat()) + (
        ''.join(random.sample(string.ascii_letters, 8)))


def get_s3_upload_path(project_id, file_type, upload_id, filename):
    return os.path.join("projects", project_id, file_type, upload_id, filename)


def get_file_size(file):
    try:
        return os.path.getsize(file)
    except Exception:
        return 0


def get_apk_details(apk_path):
    try:
        apk = APK(apk_path)
        app_identifier = apk.package
        app_version = str(apk.version_name) + " (" + str(apk.version_code) + ")"
        app_name = apk.get_app_name()
        return app_name, app_identifier, app_version
    except Exception:
        return "", "", ""


def get_ipa_details(ipa_path):
    def find_plist_path(zip_file):
        name_list = zip_file.namelist()
        pattern = re.compile(r'Payload/[^/]*.app/Info.plist')
        for path in name_list:
            m = pattern.match(path)
            if m is not None:
                return m.group()

    try:
        ipa_file = zipfile.ZipFile(ipa_path)
        plist_path = find_plist_path(ipa_file)
        plist_data = ipa_file.read(plist_path)
        plist_root = plistlib.loads(plist_data)
        app_name, app_identifier, app_version = "", "", ""
        if "CFBundleDisplayName" in plist_root:
            app_name = plist_root["CFBundleDisplayName"]
        if "CFBundleIdentifier" in plist_root:
            app_identifier = plist_root["CFBundleIdentifier"]
        if "CFBundleShortVersionString" in plist_root:
            app_version = plist_root["CFBundleShortVersionString"]
        return app_name, app_identifier, app_version
    except Exception as e:
        current_app.logger.info(f"Get ipa details error: {e}")
        return "", "", ""


def upload_to_s3(file_path, bucket_name, uploaded_file_name):
    try:
        print(f"Uploading {file_path} to s3 bucket {bucket_name} as {uploaded_file_name}")
        s3_client.upload_file(file_path, bucket_name, uploaded_file_name)
    except ClientError as e:
        print(f"Encounter error while uploading {file_path} to s3 bucket {bucket_name} as {uploaded_file_name}: {e}")


def download_from_s3(s3_file_name, bucket_name, downloaded_file_name):
    """
    s3_file_name: the name / path of the object you want to download in the bucket
    bucket_name: the name of the target bucket
    downloaded_file_name: the FULL path of where to save the downloaded file
    """
    try:
        print(f"Downloading {s3_file_name} from s3 bucket {bucket_name} as {downloaded_file_name}")
        s3_client.download_file(bucket_name, s3_file_name, downloaded_file_name)
    except ClientError as e:
        print(f"Encounter error while downloading {s3_file_name} from s3 bucket {bucket_name} as {downloaded_file_name}: {e}")


def get_s3_file_public_url(file_path: str):
    if not file_path:
        return None
    return f"https://{Constants.DEFAULT_BUCKET}.s3.{AWS_REGION}.amazonaws.com/{file_path}"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in Constants.ALLOWED_EXTENSIONS
