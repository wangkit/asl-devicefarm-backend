import atexit
import datetime
import requests
import sys
from flask import current_app
from apis.config.config import SCRIPT_SECRET_KEY, IS_KEEP_SCANNING_PENDING_JOB_KEY, MAXIMUM_CLOUD_DRIVER_ATTEMPT_KEY, \
    CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_KEY
from apis.model.Execution import Execution, db as execution_table
from apis.model.ExecutionQueue import ExecutionQueue, db as execution_queue_table
from apscheduler.schedulers.background import BackgroundScheduler
from sqlalchemy import asc
from apis import create_app, socketio_manager, Config
from constants.constants import Constants
from key.key import Key
from status.job_status import JobStatus

application = create_app()


def scan_queue():
    with application.app_context():
        need_scanning: Config = Config.query.filter_by(name=IS_KEEP_SCANNING_PENDING_JOB_KEY).first()
        if need_scanning and need_scanning.value:
            running_jobs = ExecutionQueue.query.filter_by(status=JobStatus.running).all()
            current_app.logger.info(f"Currently running jobs: {len(running_jobs)}")
            if len(running_jobs) == 0:
                all_pending_jobs = ExecutionQueue.query.filter_by(status=JobStatus.pending).order_by(asc(ExecutionQueue.created_at)).all()
                to_be_executed_job = None
                for q in all_pending_jobs:
                    current_app.logger.info(f"Eid: {q.execution_id}, Next run time: {q.next_run_time}, utcnow: {datetime.datetime.utcnow()}")
                    if not q.next_run_time or q.next_run_time <= datetime.datetime.utcnow():
                        to_be_executed_job = q
                        current_app.logger.info(f"To be executed job: {to_be_executed_job.execution_id}")
                        break
                if to_be_executed_job:
                    execution: Execution = Execution.query.filter_by(execution_id=to_be_executed_job.execution_id).one_or_none()
                    maximum_attempt_time = Config.query.filter_by(name=MAXIMUM_CLOUD_DRIVER_ATTEMPT_KEY).first()
                    current_app.logger.info(f"Maximum attempt time: {maximum_attempt_time.value}, Retry time: {to_be_executed_job.retry_time}")
                    current_app.logger.info(f"Exceed maximum retry: {maximum_attempt_time and to_be_executed_job.retry_time >= maximum_attempt_time.value}")
                    if maximum_attempt_time and to_be_executed_job.retry_time >= maximum_attempt_time.value:
                        # Handle if the retry time of this job has exceeded maximum as stated in config table
                        # 1. Give the execution a start time and finish time
                        execution.executed_at = datetime.datetime.utcnow()
                        execution.finished_at = datetime.datetime.utcnow()
                        execution_table.session.commit()

                        # 2. Set this job to the status finished
                        to_be_executed_job.status = JobStatus.finished
                        execution_queue_table.session.commit()
                    else:
                        current_app.logger.info(f"Is execution exist: {execution is not None}")
                        if execution:
                            r = requests.get(Constants.SAUCELAB_BASE_URL + "v1/rdc/devices/available",
                                             auth=(current_app.config['SAUCELAB_USERNAME'],
                                                   current_app.config['SAUCELAB_ACCESS_KEY']))
                            if execution.device_id in r.json():
                                current_app.logger.info(f"Begin job {q.execution_id}")
                                execution.executed_at = datetime.datetime.utcnow()
                                execution_table.session.commit()
                                r = requests.post('http://localhost:8000/api/executeJob', json={
                                    Key.secret: SCRIPT_SECRET_KEY,
                                    Key.execution_id: q.execution_id,
                                    Key.user_id: q.user_id,
                                    Key.project_id: q.project_id,
                                    Key.job_id: q.job_id,
                                })
                            else:
                                attempt_retry_timedelta: Config = Config.query.filter_by(name=CLOUD_DRIVER_CREATE_FAIL_RETRY_TIMEDELTA_SECONDS_KEY).first()
                                current_app.logger.info(f"Target device {execution.device_id} is NOT available")
                                # Add timedelta to the next run time, so that this job will be attempted again later
                                td = 300
                                if attempt_retry_timedelta:
                                    td = attempt_retry_timedelta.value
                                to_be_executed_job.next_run_time = datetime.datetime.utcnow() + datetime.timedelta(0, td)
                                to_be_executed_job.retry_time += 1
                                execution_queue_table.session.commit()


scheduler = BackgroundScheduler()
job = scheduler.add_job(scan_queue, 'interval', seconds=5)
scheduler.start()

atexit.register(lambda: scheduler.shutdown())

if __name__ == '__main__':
    print("Start local server connecting to aws db...")
    socketio_manager.run(
        app=application,
        port=8000,
        use_reloader=False,
    )
