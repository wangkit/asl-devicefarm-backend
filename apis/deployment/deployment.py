import subprocess
from flask import request, current_app
from werkzeug.utils import secure_filename
from apis.config.config import s3_client
from apis.deployment import deployment_blueprint
from constants.constants import Constants
from download import download
from key.key import Key
from response.response_status import ResponseStatus
from response.response_value import ResponseValue
from utils import utils
from utils.utils import return_json
import os


@deployment_blueprint.route('/deploy', methods=['POST'])
def deploy():
    if Key.deploy_key not in request.form:
        current_app.logger.info(f"Deployment missing key")
        return return_json(
            ResponseStatus.bad_request,
            ResponseValue.malformed_request
        )
    if Key.web_file not in request.files:
        current_app.logger.info(f"Deployment missing file")
        return return_json(
            ResponseStatus.empty_file,
            ResponseValue.empty_file
        )
    web_file = request.files[Key.web_file]
    deploy_key = request.form[Key.deploy_key]
    if deploy_key != current_app.config['DEPLOY_KEY']:
        current_app.logger.warning(f"Deployment missing key")
        return return_json(
            ResponseStatus.forbidden,
            ResponseValue.forbidden,
        )
    if web_file and utils.allowed_file(web_file.filename):
        web_filename = secure_filename(web_file.filename)
        web_file_path = os.path.join(download.file_folder, web_filename)
        d = "/var/www/html/asl-devicefarm/public-flutter/"
        web_file.save(web_file_path)
        current_app.logger.info(f"Removing all files in directory {d}")
        subprocess.Popen(["rm", "-rf", "/var/www/html/asl-devicefarm/public-flutter/*"])
        current_app.logger.info(f"Received web file name {web_filename} saved in {web_file_path}")
        current_app.logger.info(f"Begin uploading {web_filename} to s3...")
        s3_client.upload_file(web_file_path, Constants.DEFAULT_BUCKET, web_filename, ExtraArgs={'ACL': 'public-read'})
        current_app.logger.info(f"Finish uploading")
        current_app.logger.info(f"Begin unzipping from {download.file_folder} to {d}...")
        subprocess.Popen(['unzip', '-o', web_filename, '-d', d], cwd=download.file_folder)
        current_app.logger.info(f"Deployment is successfully done")
        return return_json(
            ResponseStatus.success,
            ResponseValue.deploy_success,
        )
    current_app.logger.warning(f"Deployment with malformed request cannot be done")
    return return_json(
        ResponseStatus.bad_request,
        ResponseValue.malformed_request,
    )
