import datetime
import json
from dataclasses import dataclass
from sqlalchemy import func, Column, JSON, Text, Integer, DateTime
from status.project_status import ProjectStatus
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Project(db.Model):
    __tablename__ = 'projects'
    project_id = Column(Text, primary_key=True, nullable=False)
    user_id = Column(Text, nullable=False)
    project_name = Column(Text, nullable=False)
    quota = Column(Integer, nullable=False, default=0)
    status = Column(Integer, nullable=False, default=ProjectStatus.normal)
    created_at = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def as_dict(self) -> dict:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
