from apis.config.config import SAUCELAB_REGION


class Constants:
    """
    Store constant values
    """
    """
    s3 Default Bucket Name
    """
    DEFAULT_BUCKET = "iapptesting"

    """
    Allowed extensions of the uploaded file
    """
    ALLOWED_EXTENSIONS = {'zip', 'apk', 'ipa'}

    """
    General
    """
    IOS_APP = "IOS"
    ANDROID_APP = "ANDROID"
    ANDROID_PROPERTIES = "android"
    IOS_PROPERTIES = "iphone"
    EXECUTION = "EXECUTION"
    OUTPUT = "output"
    DEFAULT_TEST_TYPE = "APPIUM_NODE"
    DEFAULT_TEST_SPEC_TYPE = "APPIUM_NODE_TEST_SPEC"
    DEFAULT_TEST_PACKAGE = "TEST_PACKAGE"
    DEFAULT_FEATURE_FOLDER_PATH = "/src/test/resources/features"
    LOG_FILE = "console.log"
    PASS = "PASS"
    FAIL = "FAIL"
    ELK_PASSED = "PASSED"
    ELK_FAILED = "FAILED"
    IP_HEADER = "HTTP_X_FORWARDED_FOR"

    """
    Saucelab Base Url
    """
    SAUCELAB_BASE_URL = f"https://api.{SAUCELAB_REGION}.saucelabs.com/"

    """
    Test Video
    """
    TEST_VIDEO_ZIP_NAME = "test-videos.zip"
    TEST_VIDEO_ZIP_NAME_WITHOUT_EXTENSION = "test-videos"

    """
    Extension
    """
    TAG_INDICATOR = "@"
    EXCEL_EXTENSION = ".xlsx"
    FEATURE_EXTENSION = ".feature"

